package SenjiCorps.commons;

/**
 * Created by vrichard on 04/08/2015.
 */
public class Constantes {
    public static final int DES = 12;
    public static final int COMPETENCE_MAX = 11;
    public static final int COMPETENCE_MIN = 2;
    public static final int CARACTERISTIQUE_MAX = 12;
    public static final int CARACTERISTIQUE_MIN = 1;
}
