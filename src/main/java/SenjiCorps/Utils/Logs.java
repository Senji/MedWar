package SenjiCorps.Utils;

/**
 * Created by vrichard on 07/08/2015.
 */
public class Logs {

    public static enum Level {
        MUET,
        INFO,
        DEBUG,
    }

    public static Level levelLog = Level.MUET;
    public static int nbTabLevel = 0;

    public static void setLevelLog(Level level) {
        Logs.levelLog = level;
    }

    public static Level getLevelLog() {
        return Logs.levelLog;
    }

    public static int getNbTabLevel() {
        return nbTabLevel;
    }

    public static void setNbTabLevel(int nbTabLevel) {
        Logs.nbTabLevel = nbTabLevel;
    }

    public static void addTabLevel() {
        Logs.nbTabLevel++;
    }

    public static void removeTabLevel() {
        Logs.nbTabLevel--;
    }

    public static void log(Level level, String message, boolean ln) {
        switch (Logs.levelLog) {
            case INFO:
                if (level == Level.INFO) {
                    System.out.print(message);
                }
                break;
            case DEBUG:
                if (level == Level.DEBUG) {
                    System.out.print(message);
                }
                break;
        }
        if (ln && levelLog != Level.MUET) {
            System.out.println();
        }
    }

    public static void tabLog(Level level, String message, boolean ln, int tabLevel) {
        if (levelLog != Level.MUET) {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < tabLevel; i++) {
                strBuf.append("\t");
            }
            strBuf.append(message);
            log(level, strBuf.toString(), ln);
        }
    }

    public static void logNivSup(Level level, String message, boolean ln) {
        if (levelLog != Level.MUET) {
            tabLog(level, message, ln, nbTabLevel++);
        }
    }

    public static void logWithTab(Level level, String message, boolean ln) {
        if (levelLog != Level.MUET) {
            tabLog(level, message, ln, Logs.nbTabLevel);
        }
    }
}
