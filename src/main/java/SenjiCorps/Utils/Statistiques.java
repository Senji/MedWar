package SenjiCorps.Utils;

import SenjiCorps.commons.Constantes;

import java.util.ArrayList;

/**
 * Created by vrichard on 07/08/2015.
 */
public class Statistiques {
    protected class Round {
        int nbMort = 0, nbAttaque = 0, nbAttRate = 0, nbDefense = 0, nbDefRate = 0,
                nbBlessure = 0, nbBlessRate = 0, nbResistance = 0, nbResRate = 0;
        ArrayList<Integer> statAttaque, statDefense, statBlessure, statResistance, statMort;

        protected Round() {
            this.statAttaque = new ArrayList<Integer>(Constantes.DES);
            this.statDefense = new ArrayList<Integer>(Constantes.DES);
            this.statBlessure = new ArrayList<Integer>(Constantes.DES);
            this.statResistance = new ArrayList<Integer>(Constantes.DES);
            this.statMort = new ArrayList<Integer>(Constantes.DES);
        }

        protected void addAttaque(int attaque) {
            if (attaque >= 0) {
                nbAttaque++;
                statAttaque.set(attaque, statAttaque.get(attaque) + 1);
            } else {
                nbAttRate++;
            }
        }

        protected void addDefense(int defense) {
            if (defense >= 0) {
                nbDefense++;
                statDefense.set(defense, statDefense.get(defense) + 1);
            } else {
                nbDefRate++;
            }
        }

        protected void addBlessure(int blessure) {
            if (blessure >= 0) {
                nbBlessure++;
                statBlessure.set(blessure, statBlessure.get(blessure) + 1);
            } else {
                nbBlessRate++;
            }
        }

        protected void addResistance(int resistance) {
            if (resistance >= 0) {
                nbResistance++;
                statResistance.set(resistance, statResistance.get(resistance) + 1);
            } else {
                nbDefRate++;
            }
        }

        protected void addMort() {
            nbMort++;
        }
    }

    private static ArrayList<Round> rounds = null;

    public Statistiques() {
        rounds = new ArrayList<Round>();
    }

    private static void initRounds(){
        if (rounds == null) {
            rounds = new ArrayList<Round>();
        }
    }

    public static void addAttaque(int numRound, int valeur) {
        initRounds();
        if (rounds.size() > numRound) {
            rounds.get(numRound).addAttaque(valeur);
        }
    }

    public static void addDefense(int numRound, int valeur) {
        initRounds();
        if (rounds.size() > numRound) {
            rounds.get(numRound).addDefense(valeur);
        }
    }

    public static void addBlessure(int numRound, int valeur) {
        initRounds();
        if (rounds.size() > numRound) {
            rounds.get(numRound).addBlessure(valeur);
        }
    }

    public static void addResistance(int numRound, int valeur) {
        initRounds();
        if (rounds.size() > numRound) {
            rounds.get(numRound).addResistance(valeur);
        }
    }

    public static void addMort(int numRound) {
        if (rounds.size() > numRound) {
            rounds.get(numRound).addMort();
        }
    }

    public static void addAttaqueLastRnd(int valeur) {
        addAttaque(rounds.size() - 1, valeur);
    }

    public static void addDefenseLastRnd(int valeur) {
        addDefense(rounds.size() - 1, valeur);
    }

    public static void addBlessureLastRnd(int valeur) {
        addBlessure(rounds.size() - 1, valeur);
    }

    public static void addResistanceLastRnd(int valeur) {
        addResistance(rounds.size() - 1, valeur);
    }

    public static void addMortLastRnd() {
        addMort(rounds.size() - 1);
    }
}
