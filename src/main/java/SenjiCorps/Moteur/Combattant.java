package SenjiCorps.Moteur;

import SenjiCorps.commons.TypeAttaque;
import SenjiCorps.commons.TypeDegat;

/**
 * Created by vrichard on 03/08/2015.
 */
public interface Combattant {

    int attaque();

    int blessure();

    int defense(TypeAttaque typeAttaque, int valeur);

    int gestionDegats(TypeDegat typeDegat, int valeur);

    void startCapaciteSpeciale();

    void capaciteSpeciale(Groupe cible);

    void stopCapaciteSpeciale();
}
