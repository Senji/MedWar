package SenjiCorps.Moteur;

import SenjiCorps.commons.*;

import java.util.Date;
import java.util.Random;

/**
 * Created by vrichard on 04/08/2015.
 */
public class CombatUtils {
    private static Random random = null;

    private static void initRand() {
        if (random == null) {
            random = new Random((new Date()).getTime());
        }
    }

    private static boolean testSupEgalZero(int value) {
        if (value >= 0) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean testSubEgalZero(int value) {
        if (value <= 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int rangTestCompetence(int rang) {
        initRand();
        int test = random.nextInt(Constantes.DES) + 1;
        System.out.println(" --> Test Competence : ( "+rang+" - "+test+" ) " + (rang-test));
        return rang - test;
    }

    public static boolean competenceReussie(int value) {
        return testSupEgalZero(value);
    }

    public static int rangTestCaracteristique(int rang) {
        initRand();
        int test = random.nextInt(Constantes.DES) + 1;
        System.out.println(" --> Test Caractéristique : ( "+rang+" - "+test+" ) " + (rang-test));
        return rang - test;
    }

    public static boolean caracteristiqueReussie(int value) {
        return testSupEgalZero(value);
    }

    public static double resolveBonusDefense(TypeAttaque typeAttaque, TypeDefense typeDefense) {
        double bonus = 0.0;
        switch (typeAttaque) {
            case MELEE:
                switch (typeDefense) {
                    case PARADE:
                        bonus = 1;
                        break;
                    case ESQUIVE:
                        bonus = 1;
                        break;
                }
                break;
            case DISTANCE:
                switch (typeDefense) {
                    case PARADE:
                        bonus = 0.5;
                        break;
                    case ESQUIVE:
                        bonus = 2;
                        break;
                }
        }
        if (bonus > 1) {
            System.out.println(" -> Un bonus de défense de " + bonus + " est fourni.");
        } else if (bonus < 1) {
            System.out.println(" -> Un malus de défense de " + bonus + " est fourni.");
        }
        return bonus;
    }

    public static double resolveBonusResistance(TypeDegat typeDegat, TypeArmure typeArmure) {
        double bonus = 0.0;
        switch (typeDegat) {
            case PHYSIQUE:
                switch (typeArmure) {
                    case PHYSIQUE:
                        bonus = 1;
                        break;
                    case MAGIQUE:
                        bonus = 0.75;
                        break;
                }
                break;
            case MAGIQUE:
                switch (typeArmure) {
                    case PHYSIQUE:
                        bonus = 0.5;
                        break;
                    case MAGIQUE:
                        bonus = 1;
                        break;
                }
        }
        if (bonus > 1) {
            System.out.println(" -> Un bonus de résistance de " + bonus + " est fourni.");
        } else if (bonus < 1) {
            System.out.println(" -> Un malus de résistance de " + bonus + " est fourni.");
        }
        return bonus;
    }
}
