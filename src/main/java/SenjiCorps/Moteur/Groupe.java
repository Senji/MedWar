package SenjiCorps.Moteur;

import SenjiCorps.Race.Elfe;
import SenjiCorps.Race.Humain;
import SenjiCorps.commons.RacePossible;

/**
 * Created by vrichard on 03/08/2015.
 */
public class Groupe {
    private Combattant combattant;
    private int nombre;

    private Groupe() {
    }

    private Groupe(Combattant combattant, int nombre){
        this.combattant = combattant;
        this.nombre = nombre;
    }

    public Combattant getCombattant() {
        return combattant;
    }

    public int getNombre() {
        return nombre;
    }

    public static Groupe createGroupe(RacePossible race, int masse) {
        switch (race){
            case HUMAIN:
                return new Groupe(new Humain(), masse);
            case ELFE:
                return new Groupe(new Elfe(), masse);
            default:
                return new Groupe(new Humain(), masse);
        }
    }
}
