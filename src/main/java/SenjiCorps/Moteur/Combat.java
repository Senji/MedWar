package SenjiCorps.Moteur;

import SenjiCorps.Race.Race;

/**
 * Created by vrichard on 03/08/2015.
 */
public class Combat {

    public static int round(Groupe attaquant, Groupe cible) {
        Race attaquante = (Race) attaquant.getCombattant();
        Race defenseuse = (Race) cible.getCombattant();
        int nbMort = 0, nbTouche = 0, nbDefense = 0, nbBlessure = 0, nbResistance = 0;

        for (int i = 0; i < attaquant.getNombre(); i++) {

            int resultatAttaque = attaquante.attaque();
            System.out.println(attaquante.getNom() + " n°" + i + " attaque et fait " + resultatAttaque);

            if (resultatAttaque >= 0) {
                // Si l'attaquant arrive à manier son arme.
                nbTouche++;

                int resultatDefense = defenseuse.defense(attaquante.getTypeAttaque(), resultatAttaque);
                System.out.println("Un " + defenseuse.getNom() + " se défend et fait " + resultatDefense);

                if (resultatDefense < 0) {
                    // Si le défenseur n'a su éviter l'attaque.

                    int resultatBlessure = attaquante.blessure();
                    System.out.println(attaquante.getNom() + " essaye de blesser et fait " + resultatBlessure);

                    if (resultatBlessure >= 0) {
                        // Si l'attaquant à frapper assez fort.
                        nbBlessure++;

                        int resultatDegats = defenseuse.gestionDegats(attaquante.getTypeDegat(), resultatBlessure);
                        System.out.println(defenseuse.getNom() + " essayer de résister et fait " + resultatDegats);

                        if (resultatDegats < 0) {
                            // Si l'armure du défenseur n'a pas suffit.

                            nbMort++;
                            System.out.println("1 " + defenseuse.getNom() + " meurt");

                        } else {
                            nbResistance++;
                        }
                    }
                } else {
                    nbDefense++;
                }
            }
            System.out.println();
        }
        System.out.println("Il y a eu " + nbTouche + " touches, " + nbDefense + " defenses, " + nbBlessure + " blessures, " + nbResistance + " resistances -> "+nbMort+" futurs morts.");
        return nbMort;
    }
}
