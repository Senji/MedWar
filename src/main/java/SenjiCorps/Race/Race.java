package SenjiCorps.Race;

import SenjiCorps.Moteur.Combattant;
import SenjiCorps.commons.*;

/**
 * Created by vrichard on 03/08/2015.
 */
public abstract class Race implements Combattant {
    private String nom;
    private TypeAttaque typeAttaque;
    private TypeDegat typeDegat;
    private TypeArmure typeArmure;
    private TypeDefense typeDefense;
    private int competenceAttaque;
    private int force;
    private int competenceDefense;
    private int armure;
    private boolean specialStart = false;

    public Race(String nom, TypeAttaque typeAttaque, TypeDegat typeDegat, TypeArmure typeArmure, TypeDefense typeDefense) {
        this.nom = nom;
        this.typeAttaque = typeAttaque;
        this.typeDegat = typeDegat;
        this.typeArmure = typeArmure;
        this.typeDefense = typeDefense;
        this.competenceAttaque = 0;
        this.force = 0;
        this.competenceDefense = 0;
        this.armure = 0;
    }

    public Race(String nom, TypeAttaque typeAttaque, TypeDegat typeDegat, TypeArmure typeArmure, TypeDefense typeDefense, int competenceAttaque, int force, int competenceDefense, int armure) {
        this(nom, typeAttaque, typeDegat, typeArmure, typeDefense);
        this.competenceAttaque = competenceAttaque;
        this.force = force;
        this.competenceDefense = competenceDefense;
        this.armure = armure;
    }

    public String getNom() {
        return nom;
    }

    public TypeAttaque getTypeAttaque() {
        return typeAttaque;
    }

    public TypeDegat getTypeDegat() {
        return typeDegat;
    }

    public TypeArmure getTypeArmure() {
        return typeArmure;
    }

    public int getCompetenceAttaque() {
        return competenceAttaque;
    }

    public TypeDefense getTypeDefense() {
        return typeDefense;
    }

    public void setCompetenceAttaque(int competenceAttaque) {
        if (competenceAttaque > Constantes.COMPETENCE_MAX) {
            this.competenceAttaque = Constantes.COMPETENCE_MAX;
        } else if (competenceAttaque < Constantes.COMPETENCE_MIN) {
            this.competenceAttaque = Constantes.COMPETENCE_MIN;
        } else {
            this.competenceAttaque = competenceAttaque;
        }
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        if (force > Constantes.CARACTERISTIQUE_MAX) {
            this.force = Constantes.CARACTERISTIQUE_MAX;
        } else if (force < Constantes.CARACTERISTIQUE_MIN) {
            this.force = Constantes.CARACTERISTIQUE_MIN;
        } else {
            this.force = force;
        }
    }

    public int getCompetenceDefense() {
        return competenceDefense;
    }

    public void setCompetenceDefense(int competenceDefense) {
        if (competenceDefense > Constantes.COMPETENCE_MAX) {
            this.competenceDefense = Constantes.COMPETENCE_MAX;
        } else if (competenceDefense < Constantes.COMPETENCE_MIN) {
            this.competenceDefense = Constantes.COMPETENCE_MIN;
        } else {
            this.competenceDefense = competenceDefense;
        }
    }

    public int getArmure() {
        return armure;
    }

    public void setArmure(int armure) {
        if (armure > Constantes.CARACTERISTIQUE_MAX) {
            this.armure = Constantes.CARACTERISTIQUE_MAX;
        } else if (armure < Constantes.CARACTERISTIQUE_MIN) {
            this.armure = Constantes.CARACTERISTIQUE_MIN;
        } else {
            this.armure = armure;
        }
    }

    public boolean isSpecialStart() {
        return specialStart;
    }

    public void setSpecialStart(boolean specialStart) {
        this.specialStart = specialStart;
    }

    @Override
    public String toString() {
        return "Race{" +
                "nom='" + nom + '\'' +
                ", typeAttaque=" + typeAttaque +
                ", typeDegat=" + typeDegat +
                ", typeArmure=" + typeArmure +
                ", competenceAttaque=" + competenceAttaque +
                ", force=" + force +
                ", competenceDefense=" + competenceDefense +
                ", armure=" + armure +
                '}';
    }
}
