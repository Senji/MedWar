package SenjiCorps.Race;

import SenjiCorps.Moteur.CombatUtils;
import SenjiCorps.Moteur.Groupe;
import SenjiCorps.commons.*;

/**
 * Created by vrichard on 04/08/2015.
 */
public class Elfe extends Race {

    public Elfe() {
        this("Elfe", TypeAttaque.DISTANCE, TypeDegat.PHYSIQUE, TypeArmure.PHYSIQUE, TypeDefense.ESQUIVE);
        this.setCompetenceAttaque((Constantes.DES / 2) + 2);
        this.setCompetenceDefense((Constantes.DES / 2) + 2);
        this.setForce((Constantes.DES / 2) - 2);
        this.setArmure((Constantes.DES / 2) - 2);
    }

    private Elfe(String nom, TypeAttaque typeAttaque, TypeDegat typeDegat, TypeArmure typeArmure, TypeDefense typeDefense) {
        super(nom, typeAttaque, typeDegat, typeArmure, typeDefense);
    }

    public int attaque() {
        return CombatUtils.rangTestCompetence(getCompetenceAttaque());
    }

    public int blessure() {
        return CombatUtils.rangTestCaracteristique(getForce());
    }

    public int defense(TypeAttaque typeAttaque, int valeur) {
        double bonus = CombatUtils.resolveBonusDefense(typeAttaque, getTypeDefense());
        return CombatUtils.rangTestCompetence((int) (getCompetenceDefense() * bonus));
    }

    public int gestionDegats(TypeDegat typeDegat, int valeur) {
        double bonus = CombatUtils.resolveBonusResistance(typeDegat, getTypeArmure());
        return CombatUtils.rangTestCaracteristique((int) (getArmure() * bonus));
    }

    public void startCapaciteSpeciale() {
        this.setSpecialStart(true);
    }

    public void capaciteSpeciale(Groupe cible) {

    }

    public void stopCapaciteSpeciale() {
        this.setSpecialStart(false);
    }
}
