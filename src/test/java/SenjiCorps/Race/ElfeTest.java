package SenjiCorps.Race;

import SenjiCorps.commons.TypeArmure;
import SenjiCorps.commons.TypeAttaque;
import SenjiCorps.commons.TypeDegat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vrichard on 04/08/2015.
 */
public class ElfeTest {

    public Elfe test;

    @Before
    public void setUp() throws Exception {
        test = new Elfe();
    }

    @Test
    public void testCreation() throws Exception {
        Assert.assertEquals("Elfe", test.getNom());
        Assert.assertEquals(TypeAttaque.DISTANCE, test.getTypeAttaque());
        Assert.assertEquals(TypeDegat.PHYSIQUE, test.getTypeDegat());
        Assert.assertEquals(TypeArmure.PHYSIQUE, test.getTypeArmure());
    }

    @Test
    public void testAttaque() throws Exception {

    }

    @Test
    public void testBlessure() throws Exception {

    }

    @Test
    public void testDefense() throws Exception {

    }

    @Test
    public void testGestionDegats() throws Exception {

    }

    @Test
    public void testStartCapaciteSpeciale() throws Exception {

    }

    @Test
    public void testCapaciteSpeciale() throws Exception {

    }

    @Test
    public void testStopCapaciteSpeciale() throws Exception {

    }
}