package SenjiCorps.Moteur;

import SenjiCorps.commons.*;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vrichard on 04/08/2015.
 */
public class CombatUtilsTest extends TestCase {

    @Test
    public void testRangTestCompetence() throws Exception {
        int valueTest = 0;
        int test = CombatUtils.rangTestCompetence(valueTest);
        Assert.assertTrue(test < 0);

        valueTest = 1;
        test = CombatUtils.rangTestCompetence(valueTest);
        Assert.assertTrue(test <= 0);

        valueTest = Constantes.DES;
        test = CombatUtils.rangTestCompetence(valueTest);
        Assert.assertTrue(test >= 0);

        valueTest = Constantes.DES + 1;
        test = CombatUtils.rangTestCompetence(valueTest);
        Assert.assertTrue(test > 0);
    }

    @Test
    public void testCompetenceReussie() throws Exception {
        int valueTest = 0;
        boolean test = CombatUtils.competenceReussie(valueTest);
        Assert.assertTrue(test);

        valueTest = 1;
        test = CombatUtils.competenceReussie(valueTest);
        Assert.assertTrue(test);

        valueTest = -1;
        test = CombatUtils.competenceReussie(valueTest);
        Assert.assertFalse(test);


        valueTest = 0;
        int intTest = CombatUtils.rangTestCompetence(valueTest);
        test = CombatUtils.competenceReussie(intTest);
        Assert.assertFalse(test);

        valueTest = Constantes.DES;
        intTest = CombatUtils.rangTestCompetence(valueTest);
        test = CombatUtils.competenceReussie(intTest);
        Assert.assertTrue(test);

        valueTest = Constantes.DES + 1;
        intTest = CombatUtils.rangTestCompetence(valueTest);
        test = CombatUtils.competenceReussie(intTest);
        Assert.assertTrue(test);
    }

    @Test
    public void testRangTestCaracteristique() throws Exception {
        int valueTest = 0;
        int test = CombatUtils.rangTestCaracteristique(valueTest);
        Assert.assertTrue(test < 0);

        valueTest = 1;
        test = CombatUtils.rangTestCaracteristique(valueTest);
        Assert.assertTrue(test <= 0);

        valueTest = Constantes.DES;
        test = CombatUtils.rangTestCaracteristique(valueTest);
        Assert.assertTrue(test >= 0);

        valueTest = Constantes.DES + 1;
        test = CombatUtils.rangTestCaracteristique(valueTest);
        Assert.assertTrue(test > 0);
    }

    @Test
    public void testCaracteristiqueReussie() throws Exception {
        int valueTest = 0;
        boolean test = CombatUtils.caracteristiqueReussie(valueTest);
        Assert.assertTrue(test);

        valueTest = 1;
        test = CombatUtils.caracteristiqueReussie(valueTest);
        Assert.assertTrue(test);

        valueTest = -1;
        test = CombatUtils.caracteristiqueReussie(valueTest);
        Assert.assertFalse(test);


        valueTest = 0;
        int intTest = CombatUtils.rangTestCaracteristique(valueTest);
        test = CombatUtils.caracteristiqueReussie(intTest);
        Assert.assertFalse(test);

        valueTest = Constantes.DES;
        intTest = CombatUtils.rangTestCaracteristique(valueTest);
        test = CombatUtils.caracteristiqueReussie(intTest);
        Assert.assertTrue(test);

        valueTest = Constantes.DES + 1;
        intTest = CombatUtils.rangTestCaracteristique(valueTest);
        test = CombatUtils.caracteristiqueReussie(intTest);
        Assert.assertTrue(test);
    }


    @Test
    public void testResolveBonusDefense() throws Exception {
        Assert.assertEquals(1, CombatUtils.resolveBonusDefense(TypeAttaque.MELEE, TypeDefense.PARADE), 0);
        Assert.assertEquals(1, CombatUtils.resolveBonusDefense(TypeAttaque.MELEE, TypeDefense.ESQUIVE), 0);

        Assert.assertEquals(0.5, CombatUtils.resolveBonusDefense(TypeAttaque.DISTANCE, TypeDefense.PARADE), 0);
        Assert.assertEquals(2, CombatUtils.resolveBonusDefense(TypeAttaque.DISTANCE, TypeDefense.ESQUIVE), 0);
    }

    @Test
    public void testResolveBonusResistance() throws Exception {
        Assert.assertEquals(1, CombatUtils.resolveBonusResistance(TypeDegat.PHYSIQUE, TypeArmure.PHYSIQUE), 0);
        Assert.assertEquals(0.75, CombatUtils.resolveBonusResistance(TypeDegat.PHYSIQUE, TypeArmure.MAGIQUE), 0);

        Assert.assertEquals(0.5, CombatUtils.resolveBonusResistance(TypeDegat.MAGIQUE, TypeArmure.PHYSIQUE), 0);
        Assert.assertEquals(1, CombatUtils.resolveBonusResistance(TypeDegat.MAGIQUE, TypeArmure.MAGIQUE), 0);
    }
}