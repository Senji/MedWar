package SenjiCorps.Moteur;

import SenjiCorps.commons.RacePossible;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vrichard on 06/08/2015.
 */
public class CombatTest {

    Groupe a;
    Groupe b;

    @Before
    public void setUp() throws Exception {
        this.a = Groupe.createGroupe(RacePossible.ELFE, 100);
        this.b = Groupe.createGroupe(RacePossible.HUMAIN, 100);
    }

    @Test
    public void testRound() throws Exception {
        int result = Combat.round(a, b);
        Assert.assertTrue(result >= 0);

        result = Combat.round(b, a);
        Assert.assertTrue(result >= 0);
    }
}