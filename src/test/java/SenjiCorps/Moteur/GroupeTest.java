package SenjiCorps.Moteur;

import SenjiCorps.Race.Race;
import SenjiCorps.commons.RacePossible;
import SenjiCorps.commons.TypeArmure;
import SenjiCorps.commons.TypeAttaque;
import SenjiCorps.commons.TypeDegat;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vrichard on 04/08/2015.
 */
public class GroupeTest extends TestCase {

    @Test
    public void testCreateGroupe() throws Exception {
        int nombreRace = RacePossible.values().length;
        int nbRaceTestee = 0;


        Groupe test = Groupe.createGroupe(RacePossible.HUMAIN, 10);
        Race raceTest = (Race) test.getCombattant();
        Assert.assertEquals("Humain", raceTest.getClass().getSimpleName());
        Assert.assertEquals(10, test.getNombre());
        nbRaceTestee++;


        test = Groupe.createGroupe(RacePossible.ELFE, 10);
        raceTest = (Race) test.getCombattant();
        Assert.assertEquals("Elfe", raceTest.getClass().getSimpleName());
        Assert.assertEquals(10, test.getNombre());
        nbRaceTestee++;


        Assert.assertEquals(nbRaceTestee, nombreRace);
    }
}